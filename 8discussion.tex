\chapter{Discussion}
\label{chapter:discussion}

\section{RQ1}

The goal of RQ1 was to find depicting metrics to measure the maintainability of the legacy code base in the case company.

Two artifacts were created as an answer to RQ1: A list of relevant metrics to measure the maintainability of the legacy code base of the case company. To concertize the listed metrics a system was implemented that generates metrics data for the three top priority metrics of the list: the use of global state, the use of a suboptimal in-house data structure and bad naming of functions and variables. The system also gives a single value for the maintainability of the analyzed file. This value is the maintainability index and is calculated from the results of the three metrics. Finally the results are prioritized based on the file activity of each file and their maintainability index. Recurrently active files with the biggest maintainability index are prioritized highest and inactive files with low maintainability index values the lowest.

\citet{antinyan2014monitoring} concluded in his case research that using just two sorts of metrics, cyclomatic complexity and the amount of changes of a file, were sufficient to indicate problematic places in the code base. The minimal set of metrics in this research can be compared with this set. Instead of cyclomatic complexity it consists of three low level company specific major problems in the code supplemented with the file activity calculated in a similar manner as described by \citet{schulte2014active}.

Although \citet{heitlager2007practical} prefer technology independent metrics, the results of the sessions at the case company show that the biggest issues at the case company are specific to the bad technology and even case company specific practices they have been following in their legacy code base. To offer more precise results of the actual problems the chosen metrics are not technology independent, but such that measure specifically the most relevant problems in the case company's source code.

Based on the evaluation done at the case company the three chosen metrics and the maintainability index are relevant. The maintainability index's formula and its weights of each metric needs to be refined and justified in order not to seem biased. Prioritizing the results based on file activity has shown promise in many case studies \citep{schulte2014active, shihab2012industrial, antinyan2014monitoring} and was also received well at the case company. When working with a code base as large as at the case company with its over 14 thousand files of Fortran code, it is crucial to be able to prioritize the files and find those whose improvement would most benefit the maintainability of the software.

Whether file activity should be a part of RQ1 or RQ2 is debatable. It is an additional measure and hence part of RQ1, but its purpose is to prioritize those files which are modified regularly. Hence it is a tool for RQ2 and the usage of the metrics by making the metrics data more useful by providing an easy list of top priority files to refactor in order to most improve the overall maintainability of the code base.

The final evaluation on whether the chosen set of three metrics prioritized using the maintainability index and file activity is sufficient to draw attention to the most crucial places in the code base can be done once the metrics have been used for a longer period of time and refactoring efforts have been done using the metrics to guide the efforts.

Answers for the research question were researched with both a literature review and a case study. Literature offered a vast amount of different metrics that are either generally applicable for all programming languages or specifically the one used in the case company's legacy code base. In this research the metrics were divided into four categories: traditional software metrics, language specific coding violations, code smells and other maintainability metrics. Traditional software metrics, language specific coding violations, code smells measure the source code directly, while the other maintainability metrics such as defect density and file activity provide insight of other aspects of the code and its development and get their data from a development tools such as the version control system or the issue tracker.

Out of the results of the workshop sessions can be noted that the wanted level of abstraction for the metrics seems to change depending on the persons profession. Results from case company sessions with developers and software architects were mainly concrete, low level issues which are all considered as bad programming style adding to the technical debt of the code base. In order to improve the maintainability measured by these metrics, one just has to replace the measured items with other, more sophisticated solutions. Managers were more interested in higher level metrics such as traditional metrics together with defect density and file activity visualizing the whole code base. These were aimed for getting a rough overview on the whole code base's maintainability, but might not offer such obvious insight on how to fix the problems it is depicting.

\section{RQ2}

The goal of RQ2 was to find useful ways of including the metrics in the development teams normal development work flow.

Two artifacts were developed as an answer to RQ2: These are the list of useful deployment practices of the metrics and useful attributes it should have. Two deployment practices were prototyped that either came directly or indirectly from the list and fulfill many of the useful attributes listed. The prototyped practices fulfill different needs. One practice is a reactive one and is aimed for continuous tracking of the new changes made to the code base. The other practice is a proactive one and is aimed for getting a broad overview of the whole legacy code base in order to find its problematic spots whose refactoring would most improve the overall maintainability of the software.

Both practices were suggested at the case company but also had strong backing in literature. The reactive approach makes the metrics data available for the reviewers of changes before they are merged to the master branch of the version control system. The approach is very similar to the merging practice described by \citet{antinyan2014monitoring}. The idea of the proactive approach is to be able to systematically improve the code base quality by using the metrics as a guide to point out the most troublesome parts of the code so that these can be improved by refactoring. By improving the most relevant parts of the code the aim is to get the best value for the done work. This approach is similar to the ones described by \citet{stroggylos2007refactoring, simon2001metrics, wettel2008visually}.

In the literature these approaches were found useful and the evaluation concluded at the case company indicate that the proactive and reactive development practices of the metrics are useful. Still the final evaluation cannot be done until the practices have been taken into use.

The answers to RQ2 in literature can be categorized into five categories: practices that form a part of individual developers' work process, practices that form a part of agile teams' work process, company-wide practices, practices regarding the metrics program itself and technical solutions. The majority of the suggested ideas at the case company were technical solutions (8 out of 17). The developers seem to want to automatize the metrics and their usage as much as possible to make it a natural fit to their working process without without adding new extra steps to it. The rest of the suggested deployment ideas were team level practices (4 out of 17), company wide practices (3 out of 17) and practices regarding the metrics program itself (3 out of 17).

Although the code bases are all unique and have their own specific problems, the development processes used in software companies are mostly based on some standard such as the Scrum process. Hence the the deployment practices for incorporating metrics in the every-day work flow of developers can be generalized better to be used even as such in any other software company with a similar work process. This is supported by the fact that the majority, 71\%, of the ideas regarding useful deployment practices and their attributes that were suggested by the case company participants were also mentioned in literature.

\section{Evaluation of the validity of the research}
To evaluate the validity the validity classification scheme described by \citet{runeson2009guidelines} is used.

\subsection{Construct validity}
The construct validity measures how the research methods might pose a threat to research validity. The Design science research method that was used in the case research emphasizes the importance of research rigor and critically evaluating all implemented research artifacts. In the information gathering sessions organized at the case company the group of participants was large. All developers working with the legacy code base and the key managers related to its development were invited and 70\% of them participated.

The ARCA-tool and the used retrospective method give every participant good possibilities to participate as noted by \citet{lehtinen2014tool} and by \citet{bjornson2009improving} for sessions including silent writing instead of only group discussions. The fact that not all invited persons were able to participate poses a slight risk that the results might have had a slightly different emphasis if all had participated.

It is probable that the participants were the ones that were the most interested in the subject. The ones that did not participate might have had a different view on the researched problem.

In the literature review no strict, formal method was used. The search was performed by exploratory methods followed by snowballing \citep{jalali2012systematic}. The exploratory method consisted of searching for related keywords such as "measuring software maintainability'', "technical debt'' and "software metrics deployment'' in different search engines and databases such as Google Scholar, Scopus, IEEE Xplore, ACM and ScienceDirect, . The abstracts of articles with promising topics were read and if they seemed relevant the rest of the article was read in more detail. When relevant articles were found, the Snowballing method \citep{jalali2012systematic} was used to find more articles on the same subject. This meant that articles that referenced (forward snowballing) or that were referenced by the read article (backward snowballing) were browsed and those potentially relevant ones read.

A more systematic way would have been to replace the exploratory method with a systematic literature review \citep{kitchenham2009systematic}, but this would have also been a very heavy process and would taken time from the case research and hence reduced its scope. For this reason the exploratory approach combined with snowballing was chosen.

Due to less strict and undocumented the literature review method there is a risk that the set of articles used in the literature review were affected by the conductors personal preference. On the other hand due to the more lightweight search process it was possible to use a broader set of search engines, databases and search terms.

\subsection{Internal validity}
Internal validity is relevant when researching causal relationships. There is a risk that a possible additional cause for the investigated behavior might be relevant but is not noted by the researcher.

In the manner the case study was conducted there is little risk for mis-evaluations, since the evaluation is basically done collaboratively by the case company participants on what makes their personal maintenance efforts of the legacy code base difficult and how the metrics data should be deployed to be useful. The artifact of the sessions are prioritized lists of the most important maintainability issues to measure and how to use these metrics. For RQ1 the three top voted issues were the ones that were implemented in the artifact. 

The internal validity of the results for RQ2 is harder to verify, since the prototyped reactive and proactive deployment approaches of the metrics were not simply taken from the top 2 of the list of the workshop results. 

Later the usefulness of the implemented artifacts were also evaluated by case company personnel.

\subsection{External validity}
External validity measures the level of which the results of the research are generalizable and whether they are interesting also outside the specific researched case context.

Apart from the three company specific code measures, the results should be generally applicable to any Scrum organization that aims to control and systematically repay its quality debt. The organization can use a similar approach as described in this thesis to find out their unique set of top priority technical debt issues to measure. The process was applied in a multi-site Scrum organization and hence applied techniques that fit well distributed teams. The deployment practices utilize cloud services making them useful to distributed teams. This is discussed in more detail in the conclusions chapter. The results of the literature review are generalizable, since they offer a broad view of the current literature on the subject of measuring software maintainability.

The purpose of the case study was to find those results that are relevant specifically for the case company and are hence not generalizable as such. What can be generalized from the case study results though is that according to this study it does make sense to find out any company's specific needs to find out the most relevant metrics suite to fit their needs. The process used to find these needs suited the purpose well and could be easily used in other companies as well.

To make this conclusion more scientifically significant it would make sense to do an evaluation of the actual use of the metrics at the case company and to repeat the same study in different companies. Then the results might show whether it is useful to use company specific metrics for measuring the maintainability of their software products.

\subsection{Reliability}
Reliability measures how much the results were dependent of the person conducting the research. Can another person repeating the same steps expect to find the same solutions?

All steps of the research are well-documented. Hence following the documentation the results should be more or less the same for any researcher. 

The researcher acted as the facilitator of the retrospective sessions at the case company, which might slightly affect the results of the sessions, compared to if someone else had conducted them. But the effect will be minimal as \citet{bjornson2009improving} noted in their sessions using causal mapping. The used method is similar to the method of \citet{bjornson2009improving}.

It is possible that the results of the evaluation session at the case company were slightly affected by the fact that the research worker was the one presenting the results. Due to this they might have given slightly better evaluations in order to not seem rude. Yet it probably did not have a very big effect, since the evaluation was conducted anonymously in a written, indirect manner. Also it was emphasized in the session that honest feedback is valued and that it is the only way the metrics can later be improved in the right direction to be the most useful for the case company.
