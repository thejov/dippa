\chapter{Maintainability}
\label{chapter:maintainability}

This chapter presents the key terminology of the thesis. The concept of maintainability is defined and put into context in the software life cycle. Also the related terms of technical debt and legacy code are defined.

\section{Software life cycle}
Although it is traditionally thought that any work on a software after its initial release is software maintenance many more sophisticated software life cycle models like the staged model have been introduced \citep{rajlich2000staged}. This model consists of the following five stages:

The initial development stage covers all development done before the initial release of the software. The evolution stage follows the initial release and may include major modifications to the software. The servicing stage start when the architecture of code base has become too complex and hard to maintain or the company is lacking skilled personnel to do it. In this phase only minor modifications, mostly bug fixes, are done and touching the code is mostly avoided by using wrappers. \citep{rajlich2000staged}

When the software company decides to do no more servicing, the software will move to the phaseout stage. The software company will be generating revenue with the software for as long as it can without doing any more modifications to it. All problems are solved using workarounds. Finally the software will enter the closedown stage where the company stops supporting the software and possibly encourages its users to start using the replacing software. \citep{rajlich2000staged}

Hence not all stages after the initial development should be regarded as software maintenance, only the stages in which active development of the software's source code take place. This is clarified in Figure \ref{fig:staged_software_life_cycle}.

\begin{figure}[!htbp]	
	\begin{center}
		\includegraphics[width=\textwidth]{images/staged_software_life_cycle.png}
    		\caption{A modified illustration of the staged software life cycle \citep{rajlich2000staged} emphasizing in which part of the life cycle software maintenance takes place.}
    		\label{fig:staged_software_life_cycle}
  	\end{center}
\end{figure}

\section{Maintainability, technical debt and legacy code}
The terms software maintainability, technical debt and legacy code are all closely related. Technical debt can be seen as a part of maintainability and legacy code as code that has serious problems with maintainability.

Maintainability and evolvability practically mean the same thing \citep{mantyla2009software}. Software maintainability describes the ease of keeping a software in a current, stable state \citep{mantyla2009software} including activities like bug fixing, performance improvements and the adaptation of the software to new environments \citep{ieee1990ieee}, hence making software maintenance more productive \citep{frost1985software}. Evolvability emphasizes the ability to further develop a software more clearly and is hence preferred by some \citep{mantyla2009software}.

Technical debt was first introduced by \citet{cunningham1992wycash} as a metaphor for developers choosing to do their implementation work by "quick and dirty" hacking under time pressure and hence setting up with a technical debt, which is like a financial debt. The organization has to pay interest regularly in form of less productive development work, i.e. by spending more effort on feature development and bug fixing \citep{nugroho2011empirical}. They can also choose to pay down the principal by refactoring away the unfavorable "quick and dirty" design. By investing in reducing the principal the organization reduces the future interest payments \citep{fowler2003technical}. In other words technical debt describes bad design choices in the code base, which make further development less productive. To boost the productivity one has to improve the bad design.

Since the original definition the use of the term technical debt has become more ambiguous and it has been used covering new areas of software and its development process that are not directly related to the technical quality of a software \citep{kruchten2012technical}. For this reason \citet{kruchten2012technical} suggested a reduced and clarified definition of technical debt, which describes technical debt only as the part of the technical debt landscpe that is mostly invisible to others than developers. This includes parts architectural debt, structural debt, test debt, documentation debt, code complexity, coding style violations, low internal quality and code smells. Together with defects and low external quality, technical debt forms a part of the quality issues affecting the maintainability of a software system. This is the definition used to describe technical debt in the context of the thesis.

Generally legacy code is understood as incomprehensible code that is hard to change \citep{feathers2004working}. Hence legacy code is code that is laborious to maintain and probably suffers from technical debt. Michael C. Feathers defines legacy code more radically as simply code without tests \citep{feathers2004working} in other words having test debt. The reasoning behind this is that when making changes to code without tests it is impossible to verify that the changes do not create bugs in the software.  No matter how clean the architecture and the code is.

\section{Improving the maintainability}
Refactoring is the process of improving the internal structure and design of a software system without changing its external behavior \citep{fowler1999refactoring}. It is the work to undo the gradual degradation of a software systems source code that tends to happen to software during its evolution phase.

Reengineering consists of reverse engineering the systems source, restructuring it, possibly moving the system on a new platform or environment or even translating the source code to a new language \citep{waters1994reverse} like suggested by \citet{fanta1999restructuring} and \citet{takeda2004legacy}.

Difference between refactoring and reengineering is the scope of the changes. Refactoring tends to include more local changes in individual source code files while reengineering usually includes bigger structural changes that affect the whole software system. Refactoring might be included as a part of the total reengineering work \citep{demeyer2002object, sommerville2011software}.