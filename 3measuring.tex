\chapter{Measuring technical debt and maintainability}
\label{chapter:measuring}

In this chapter the existing knowledge base on measuring technical debt and maintainability is researched in form of a literature review. Measuring software maintainability in the way that it truly depicts the maintenance burden of the development teams is a challenging task. Hence no single correct solution exists but many different approaches have been introduced. These range from more traditional metrics like cyclomatic complexity to code smells indicating suspicious code design. In this chapter we will first clarify the the commonly confusing concepts of measures, metrics and key performance indicators and their difference. Then we will go through different types of software quality metrics that are suggested by literature to measure technical debt and maintainability. The difference between automated metrics and manual evaluation is also shortly described.

In this thesis the different types of metrics are classified into the following categories: traditional metrics, language specific coding violations, code smells and other maintainability metrics (see Figure \ref{fig:metrics}). In the following chapter each class and its single metrics are described in more detail.

\begin{figure}[!htbp]	
	\begin{center}
		\includegraphics[width=\textwidth]{images/metrics.png}
    		\caption{Different types of maintainability metrics}
    		\label{fig:metrics}
  	\end{center}
\end{figure}

\section{Difference between measure, metric, indicators and key performance indicators}
The concepts of measure, metric, indicators and key performance indicator are commonly used interchangeably and are hence distinguished and defined here. A measure or measurement is a single point of measuring comparing to a standard \citep{ieee1983ieee}, i.e. by using a standard unit of measure like centimeters when measuring length. Examples for measurements are 3 cm, 10 Newton, 100 source lines of code, cyclomatic complexity of 20, etc.

A metric is based upon two or more measures relating them in some manner \citep{pressman2005software}, for example the change of a softwares cyclomatic complexity over time or fixed defects per 1000 lines of code. Trends can become visible in metrics.

An indicator is a metric or combination of metrics that provides actual insight onto the state of a product, process, etc. In order to do this an indicator brings context to the metric by comparing it to a specified condition \citep{ieee1990ieee}, e.g. a goal value to be attained. For example the evolution of a software's cyclomatic complexity during the year with the goal of 10, because it is commonly considered a good value.

The difference between an indicator and a key performance indicator is that the KPI's focus measuring performance aspects that are most relevant for the success of the organization \citep{parmenter2010key}. Hence they are often derived from the company's strategy. A KPI could for example measure a software's cyclomatic complexity during the year with the company's strategic goal set to the cyclomatic complexity of 10.

The related figures \ref{fig:measure}, \ref{fig:metric} and \ref{fig:indicator} visualize the given example.

\begin{figure}[!htbp]	
	\begin{center}
		\includegraphics[width=\textwidth]{images/measure.png}
    		\caption{Example for a measure: A single measure of cyclomatic complexity of a software in a single point of time.}
    		\label{fig:measure}
  	\end{center}
\end{figure}

\begin{figure}[!htbp]	
	\begin{center}
		\includegraphics[width=\textwidth]{images/metric.png}
    		\caption{Example for a metric: Cyclomatic complexity over time.}
    		\label{fig:metric}
  	\end{center}
\end{figure}

\begin{figure}[!htbp]	
	\begin{center}
		\includegraphics[width=\textwidth]{images/indicator.png}
    		\caption{Example for an indicator: Measured cyclomatic complexity over time in comparison to the feasible complexity level.}
    		\label{fig:indicator}
  	\end{center}
\end{figure}

\section{Traditional software metrics}
In the traditional software metrics this paper includes metrics like the size in lines of code (LOC) \citep{yamashita2012code}, McCabe's cyclomatic complexity \citep{mccabe1976complexity}, Halstead complexity measures \citep{halstead1977elements}, the Maintainability Index \citep{welker1995software} and unit test coverage \ref{feathers2004working}.

\begin{longtable}{p{0.25\textwidth}p{0.75\textwidth}}
\caption{Traditional code metrics introduced in literature.}\\
\textbf{Metric} & \textbf{Description} \\ \hline
LOC & Lines of code in subroutine, file, etc. \\ \hline
Cyclomatic Complexity & Amount of possible execution paths in code. Directly correlates with ease of testing, since each execution path requires its own test case. \\ \hline
Halstead Complexity measures & Measures such as program volume, difficulty to understand and coding effort calculated of the number of distinct and total operators, operands. \\ \hline
Maintainability Index & A single compound value to depict maintainability that consists of the Halstead Volume metric, cyclomatic complexity, the average number of lines of code per module and optionally the percentage of comment lines per module. \\ \hline
Unit test coverage & Indicates what part of the code base is covered by automated unit tests. \\
\end{longtable}

\subsection{LOC}

Paul McMahon's, the co-founder of a Japan based software company doorkeeper.jp, stated in his tweet \footnote{\url{https://twitter.com/pwim/status/549463179084853248}} that all code is technical debt and that some code just has a higher interest rate. The tweet was presented by Kevin Henney at the QCon London 2015 conference in his presentation "Small Is Beautiful'' \citep{henney2015small}. The presentation emphasized the importance of a small code base in keeping a software maintainable. The smaller the code base, the easier it is to grasp and hence maintain. According to this way of thinking the best metric for software maintainability is the number of lines of code of the code base.

The lines of code can be measured in different ways of which the most common way is to ignore comment lines and whitespaces and hence only count lines of code that include executable source code. This is also known as SLOC, source lines of code. The LOC measures can be total LOC of the program \citep{heitlager2007practical}, average LOC of a function or the percentages of the  distribution of different size functions classified into levels from very long to short \citep{heitlager2007practical}.

\subsection{Cyclomatic Complexity}
An often, e.g. in the Maintainability Index, used measure is average cyclomatic complexity of a function. Its relevance has been criticized since for example a big amount of trivial getter and setter functions with a minimal complexity value tend to hide the problematic functions with a high complexity value, which pose a high risk in maintenance problems. \citet{heitlager2007practical} suggests classifying complexity values into risk levels from high risk with values over 50 to low risk with values of 1 to 10. The distribution of code with different risk levels is then counted and presented as percentage of each risk level code of the whole code base. \citep{heitlager2007practical}

\subsection{Halstead Complexity measures}
The Halstead Complexity measures \citep{halstead1977elements} include for example program volume, difficulty to understand and coding effort. These are calculated from the number of distinct and total operators and operands. The Halstead Complexity measures have been criticized to be difficult to compute and suffers from the lack of a clear definition of operators and operands in different programming languages. For this reason they have not acquired a commonly accepted or widely used status \citep{heitlager2007practical, jones1994software, al2005analysis}

\subsection{Maintainability Index}
The Maintainability Index \citep{welker1995software} is a compound value of different measures: the Halstead Volume metric (HV), cyclomatic complexity (CC), the average number of lines of code per module (LOC) and optionally the percentage of comment lines per module (COM). The function is:
\begin{align}
	171 - 5.2 \ln (HV) - 0.23 CC - 16.2 \ln (LOC) + 50.0 \sin \sqrt{2.46 * COM}
\end{align}

Its function is fitted based on statistical correlations and has been criticized inter alia of being incomprehensible due to its constant values and multipliers. Also, since it is only a single value it is hard to trace down the individual problems that should be fixed in order to improve the value. Because of this developers might feel lack of control over the metric and no longer support the use of it. \citep{heitlager2007practical}

\subsection{Unit test coverage}

Unit test coverage tells how big a part of the code base is run during the unit test execution, hence what part of the code base is covered by tests. Having a good unit test coverage gives software developers the possibility to validate that their modifications to the programs source code do not have any unwanted side effects \citep{feathers2004working}.

Yet the unit test coverage does not show whether the tests are actually testing anything, or if they are only executing code without asserting the correctness of any conditions or results. One way of approaching this problem is counting the number of assert statements to give a rough idea of the test quality. \citet{heitlager2007practical}

\section{Language specific coding violations}
Coding violations are language specific bad design practices or deviations from the suggested coding standards of a language. Each programming language has their own best practices so these cannot be generalized. Since the research is focusing on measuring a code base written in the Fortran, only coding violations of this language are considered here.

Coding violations of Fortran include the use of deprecated language features or other unfavorable features of old Fortran standards, where new better ways exist. These violations are caused by the long history and language evolution of the Fortran language and are made possible by the strong backwards compatibility of the newer language versions.

\subsection{A short history of Fortran}

The first Fortran version called 'FORTRAN I' was released in 1956 \citep{backus1978history}. Since then numerous new versions have been released. The major version updates include Fortran 66, Fortran 77, Fortran 90, Fortran 2003, Fortran 2008. Fortran language evolution has strong emphasis on backwards compatibility. Old features are rarely deleted although a lot of new features are added. The possibility of doing the same thing in many different ways has lead to different dialects of Fortran programming, depending on the set of language features used. \citep{overbey2009refactoring}

\subsection{Fortran refactorings suggested by literature}

The point has been raised by \citet{overbey2009refactoring} that the different language dialects cause most problems to developers having to maintain somebody else's Fortran code, since they will have to be fluent in all different dialects and preferably also understand the advantages and disadvantages of using old or new program constructs. This is also the case in the case company. From a developers point of view an ideal situation would be to update all code base to use the same dialect, preferably the most recent and more powerful one.

Table \ref{tab:fortran_coding_violations_lit} contains a list of the Fortran coding violations, suggested refactorings and deprecated or old features listed in literature. The used sources are two conference papers \citep{mendez2010catalog, overbey2009refactoring}, the documentation of Fortran refactorings possible to perform using the Photran refactoring tool mainly developed by the University of Illinois at Urbana-Champaign \citep{overbey2014photran}, and two books \citep{haataja2007fortran, metcalf2011modern}. The refactorings are listed in order of most referenced to least referenced. The top four refactorings are suggested by all five sources, five refactorings by four sources, ten refactorings by three sources, 16 refactorings by two sources and fifteen refactorings only in one source.

\begin{longtable}{p{0.80\textwidth}|p{0.2\textwidth}}
\caption{Fortran coding violations listed in literature ordered from most referenced to least referenced.}\\
\label{tab:fortran_coding_violations_lit}
\textbf{Coding violation} & \textbf{Listed in}\ \\ \hline
Remove computed \texttt{GOTO} statement & \citep{mendez2010catalog, overbey2009refactoring, overbey2014photran, haataja2007fortran, metcalf2011modern} \\ \hline
Remove arithmetic \texttt{IF} statement & \citep{mendez2010catalog, overbey2009refactoring, overbey2014photran, haataja2007fortran, metcalf2011modern} \\ \hline
Transform \texttt{CHARACTER *} to \texttt{CHARACTER(LEN=)} declaration & \citep{mendez2010catalog, overbey2009refactoring, overbey2014photran, haataja2007fortran, metcalf2011modern} \\ \hline
Introduce \texttt{IMPLICIT NONE} & \citep{mendez2010catalog, overbey2009refactoring, overbey2014photran, haataja2007fortran} \\ \hline
Remove \texttt{REAL} type iteration index & \citep{mendez2010catalog, overbey2014photran, haataja2007fortran, metcalf2011modern} \\ \hline
Change fixed form to free form & \citep{mendez2010catalog, overbey2009refactoring, haataja2007fortran, metcalf2011modern} \\ \hline
Remove assigned \texttt{GOTO} & \citep{mendez2010catalog, overbey2014photran, haataja2007fortran, metcalf2011modern} \\ \hline
Replace old style \texttt{DO} loops & \citep{mendez2010catalog, overbey2009refactoring, overbey2014photran, haataja2007fortran} \\ \hline
Extract internal procedure & \citep{mendez2010catalog, overbey2014photran, haataja2007fortran} \\ \hline
Replace shared \texttt{DO} loop termination & \citep{mendez2010catalog, haataja2007fortran, metcalf2011modern} \\ \hline
Move \texttt{COMMON} block to \texttt{MODULE} & \citep{mendez2010catalog, overbey2009refactoring, haataja2007fortran} \\ \hline
Convert \texttt{DATA} to \texttt{PARAMETER} & \citep{mendez2010catalog, overbey2014photran, haataja2007fortran} \\ \hline
Remove \texttt{ENTRY} statements & \citep{overbey2009refactoring, haataja2007fortran, metcalf2011modern} \\ \hline
Replace \texttt{STATEMENT} functions with \texttt{INTERNAL} functions & \citep{overbey2009refactoring, haataja2007fortran, metcalf2011modern} \\ \hline
Remove branch to \texttt{END IF} & \citep{overbey2014photran, haataja2007fortran, metcalf2011modern} \\ \hline
Remove \texttt{PAUSE} statement & \citep{overbey2014photran, haataja2007fortran, metcalf2011modern} \\ \hline
Add \texttt{DIMENSION} statement & \citep{mendez2010catalog, overbey2009refactoring, haataja2007fortran} \\ \hline
Change keyword case, i.e. \texttt{FORTRAN} to \texttt{fortran} & \citep{mendez2010catalog, overbey2009refactoring, overbey2014photran} \\ \hline
Rename & \citep{mendez2010catalog, overbey2014photran} \\ \hline
Extract local variable & \citep{mendez2010catalog, overbey2014photran} \\ \hline
Encapsulate variable & \citep{mendez2010catalog, overbey2014photran} \\ \hline
Make \texttt{PRIVATE} entity \texttt{PUBLIC} & \citep{mendez2010catalog, overbey2014photran} \\ \hline
Add \texttt{ONLY} clause to \texttt{USE} statement & \citep{mendez2010catalog, overbey2014photran} \\ \hline
Remove unreferenced labels & \citep{mendez2010catalog, overbey2014photran} \\ \hline
Remove reserved words as variables & \citep{mendez2010catalog, overbey2009refactoring} \\ \hline
Remove unused local variables & \citep{mendez2010catalog, overbey2014photran} \\ \hline
Minimize \texttt{ONLY} list & \citep{mendez2010catalog, overbey2014photran} \\ \hline
Make \texttt{COMMON} variable names consistent & \citep{mendez2010catalog, overbey2014photran} \\ \hline
Remove \texttt{FORMAT} statement labels & \citep{mendez2010catalog, haataja2007fortran} \\ \hline
Replace obsolete operators: \texttt{.LT. .LE. .EQ. .NE. .GT. .GE.} & \citep{mendez2010catalog, overbey2014photran} \\ \hline
Move \texttt{SAVED} variables to \texttt{COMMON} block & \citep{mendez2010catalog, overbey2014photran} \\ \hline
Require explicit \texttt{INTERFACE} blocks & \citep{overbey2009refactoring, haataja2007fortran} \\ \hline
Assumed character length of function results & \citep{haataja2007fortran, metcalf2011modern} \\ \hline
Alternate return & \citep{haataja2007fortran, metcalf2011modern} \\ \hline
Introduce \texttt{INTENT IN / OUT} & \citep{mendez2010catalog} \\ \hline
Canonicalize keyword capitalization & \citep{mendez2010catalog} \\ \hline
Move entity between modules & \citep{mendez2010catalog} \\ \hline
Change subprogram signature & \citep{mendez2010catalog} \\ \hline
Transform to \texttt{WHILE} sentence & \citep{mendez2010catalog} \\ \hline
Delete unused \texttt{COMMON} block variable & \citep{mendez2010catalog} \\ \hline
Replace specification statements with variable's type declaration statement for \texttt{public, private, pointer, target, allocatable, intent, optional, save, dimension, parameter} & \citep{overbey2009refactoring} \\ \hline
Add identifier to \texttt{END} statement & \citep{overbey2014photran} \\ \hline
Convert between \texttt{IF} statement and \texttt{IF} construct & \citep{overbey2014photran} \\ \hline
Make \texttt{SAVE} attributes explicit & \citep{overbey2014photran} \\ \hline
Standardize statements & \citep{overbey2014photran} \\ \hline
Remove \texttt{EQUIVALENCE} statement & \citep{haataja2007fortran} \\ \hline
Remove \texttt{DOUBLE PRECISION} type & \citep{haataja2007fortran} \\ \hline
Replace \texttt{GOTO} statement with \texttt{SELECT CASE} or \texttt{IF, ELSE IF, ELSE} statement & \citep{haataja2007fortran} \\ \hline
Move \texttt{BLOCK DATA} to \texttt{MODULE} & \citep{haataja2007fortran} \\
\end{longtable}

\section{Code smells}
Code smells are suboptimal design choices that can be used as indicators of code that would benefit from refactoring \citep{fowler1999refactoring}. Code smells are for example duplicate code, long methods, long parameter list, classes that have too much or too little functionality, methods that are using more data of another class than its own. For all of the code smells introduced in \citep{fowler1999refactoring} Fowler and Beck suggest a set of refactorings to fix the situation. So code smells can be used to indicate a developer when and what to refactor.

Together with other measures code smells provide good insight on software maintainability and can be used as indicators of refactoring targets that will improve the software's maintainability \citep{yamashita2012code}. Many of the code smells and solutions suggested in \citep{fowler1999refactoring} apply as such or in a modified form to Fortran as well \citep{overbey2005refactorings}. Refactorings aimed at object oriented design can be for example applied to Fortran modules.

\begin{longtable}{p{0.25\textwidth}p{0.75\textwidth}}
\caption{Code smells introduced by \citet{fowler1999refactoring}}\\
\label{tab:code_smells}
\textbf{Code smell} & \textbf{Description} \\ \hline
Duplicated Code & Completely or mostly duplicated blocks of code. Also doing the same thing in two different ways. \\ \hline
Long Method & Methods consisting of too many lines of code. \\  \hline
Large Class & A large class having too much functionality (Swiss army knife). \\ \hline
Long Parameter List & Method parameter list too long. \\ \hline
Divergent Change & Class is often changed in different ways for varying reasons. \\ \hline
Shotgun Surgery & Same change has to be implemented in many different locations in the code. \\ \hline
Feature Envy & A method uses more data of another class than its own. \\ \hline
Data Clumps & A group of data that is commonly seen together e.g. commonly passed together as method parameters.  \\ \hline
Primitive Obsession & Using primitive data structures when the use of objects would be beneficial. \\ \hline
Switch Statements & Use of switch statements that are often duplicated into different parts of the code. \\ \hline
Parallel Inheritance Hierarchies & Duplication in inheritance hierarchies, creating subclass in one forces to create it also to the other. \\ \hline
Lazy Class & Classes that do not have much functionality. \\ \hline
Speculative Generality & Unused functionality created because of its potential need in future. \\ \hline
Temporary Field & Instance variables used only in certain cases. \\ \hline
Message Chains & Long chains of objects asking other objects for a certain value. \\ \hline
Middle Man & Using unnecessary middle men in the retrieval of data. Instead ask the relevant class directly. \\ \hline
Inappropriate Intimacy & Classes too interested in each others contents, sometimes causing bidirectional dependencies. \\ \hline
Alternative Classes with Different Interfaces & Two classes implementing practically the same functionality but have different interfaces. \\ \hline
Incomplete Library Class & Library classes that lack needed functionality. \\ \hline
Data Class & Classes that have are only holding data. All functionality is done outside of the class. \\ \hline
Refused Bequest & Superclasses that have functionality that is not needed in the subclasses. \\ \hline
Comments & Comments are often placed around inferior quality parts of the code to explain the bad design. \\
\end{longtable}

\section{Other maintainability metrics}
The already listed traditional software metrics, language specific coding violations and code smells measure focus on measuring the source code. In order to broaden the understanding of the software's maintainability's state, these metrics can be supplemented with other maintainability metrics, that utilize other aspects of the software than the source code. For example the defect density also takes into account all known defects and active files utilizes the file activity of each file in the code base. 

\subsection{Defect density}
Defect density is the number of known defects divided by the size of software at a specific time. Software size can be measured in LOC. The defect density makes it possible to compare the relative amount of defects in different size softwares or software components in order to find those which are most defect prone and would mostly benefit of improvement. \citep{westfall2013defect}

\subsection{Active files}
\citet{schulte2014active} introduces the concept of active files and recurrent active files. According to their study out of the changes done to the code base 20-40\% are made only to 2-8\% of the files, i.e. a significant proportion of the total changes is performed to a small amount of files. These files, which they call the active files, cause 60-90\% of all the defects of the software. The major part of the active files, 65-95\% were architectural hub files, which change as part of feature additions.

A file is active on a date $d$ if it has changed on that day and at least once in a selected time frame $t$ preceding the day $d$. Files are recurrently active if they are active on a day $d$ and have been changed at least once in each of the $n$ preceding time frames. \citet{schulte2014active} chose the time frame to their case research according to the products' release cadences.

Figure \ref{fig:active_files} depicts the main concepts of active files. It shows the edit history of a single file. The file is an active file, since it is edited on day $d$ and also once in the time frame between $d$ and $d - t$. The file is also a recurrent active file with $n = 3$, since apart from being a active file, it is edited once in the time frame between $d - t$ and $d - 2*t$ and between $d - 2*t$ and $d-3*t$.

\begin{figure}[!htbp]	
	\begin{center}
		\includegraphics[width=\textwidth]{images/active_files.png}
    		\caption{A file's edit history: A visualization of the concepts of active files.}
    		\label{fig:active_files}
  	\end{center}
\end{figure}

Using software change metrics indicating probable problem areas of the code base has also been suggested by \citet{shihab2012industrial}. \citet{fenton2000quantitative} also found evidence supporting the Pareto distribution of defects in software, i.e. that the major part of defects are produced by only a small number of modules. \citet{harrison1990insights} also noted how changes tend to concentrate on a small amount of files. In their research a small subset of 10\% of all changed modules accounted for over 50\% of all done changes. The importance of activeness of a part of code is also emphasized when analyzing the cost of technical debt: Technical debt in inactive code is said to be less expensive -- simply because the cost of technical debt comes in form of increased effort in future modifications \citep{ries2009embrace}.

\citet{antinyan2014monitoring} found only a moderate correlation between the number of revisions (changes in a version control system) on a file and its cyclomatic complexity. \citet{fenton2000quantitative} found no significant correlation between the complexity and fault-proneness or the size and fault-density of modules.

\citet{antinyan2014monitoring} concluded in his case research that using just two measures, cyclomatic complexity and the number of revisions was enough to find the problematic parts of code that would most benefit of improvement. The number of revisions is another way of measuring the file activity.

Since the active files seem to be the main source of defects, it seems logical to focus the maintainability improvement effort such as refactoring and raising the unit test coverage on these files and hence "get the biggest bang for the buck'' for the done effort.

\section{Problems in metrics}
The biggest challenge in using metrics must be choosing the ones that correlate the most with the actual maintainability burden of the developers. Hence a lot of research and different suggestions of sets of metrics exist to solve the problem \citep{yamashita2012code, mccabe1976complexity, halstead1977elements, welker1995software, heitlager2007practical}. \citet{fowler2003technical} claims it impossible to measure technical debt effectively. \citet{kruchten2012technical} stated that it is not possible to measure all elements of technical debt, especially bad structural or architectural design and technological gaps resulting of the inability to evolve the software according to the evolution of for example programming languages.

It has come up in discussions with the case company and another company facing similar issues with legacy code that it is hard to isolate single refactorings and their effect on the maintainability of the software to tell which of them has been worthwhile and which not. Changes in maintainability are not visible immediately and usually there have been multiple changes that could be possible causes for the change.

\section{Automatically calculated metrics vs expert judgment}
Different approaches to measuring software maintainability exist of which some are possible to calculate automatically, using for example static code analyzers, whereas on others one needs to rely on expert judgment. Combining both approaches will give a more precise and complete view on the state of the software. \citep{yamashita2012code, anda2007assessing, pizka2007effectively} A problem in using expert judgment is that it is time consuming and expensive \citep{anda2007assessing}.