\chapter{Introduction}
\label{chapter:intro}

\section{Problem statement}

Out of the software's life cycle the cost of maintenance has a major part \citep{lientz1980software}, generally over 60\% \citep{canning1972maintenance, harrison1990insights, nosek1990software}. Apart from adding new features and fixing errors, an important part of maintenance is improving the software structure and design by reengineering and refactoring it. This is done to undo the natural degradation that a code base experiences over time e.g. due to the gradual modifications done to it \citep{fowler1999refactoring}. The maintainability of source code degrades even when not modifying the code base, since new features in the programming languages raise the level of the optimum state of the source code quality \citep{pirkelbauer2010source, kruchten2012technical}. If the software quality is not regularly improved it will slow down the development teams' feature development velocity \citep{power2013understanding, fowler1999refactoring, granja1997method, arnold1989software}.

Refactoring extends the softwares lifetime \citep{arnold1989software}, keeping it longer in the evolution phase of its life cycle \citep{rajlich2000staged}, hence improving its evolvability \citep{mantyla2009software}. In his dissertation \citet{mantyla2009software} listed benefits of software evolvability. Apart from the already mentioned improved software design and longer software lifetime and better development velocity it will improve the understandability of the code \citep{fowler1999refactoring, arnold1989software}, help locate bugs \citep{fowler1999refactoring, arnold1989software}, make testing, auditing and documenting easier \citep{arnold1989software}, reduce dependency on individual developers \citep{arnold1989software}, increase job satisfaction \citep{arnold1989software} and preserve the software as an asset to the company developing it \citep{arnold1989software}.

In order to systematically improve quality we need to measure the improvements, such as refactorings or reengineerings. To do this we need to know the current state, set a goal of the desired state we want to achieve and track the progress. \citep{garcia2004towards, olsem1998incremental}

The higher level goal of the thesis is to provide the case company tools for making more informed decisions in their maintenance work. Informed decisions are based on facts instead of hunch and personal views. Ideally the metrics suite will benefit the company's maintenance work in three ways. To begin with, the metrics suite can be used for tracking the progress of the company's efforts in improving its software's maintainability towards a predefined goal. Secondly, fitting metrics show the impact of done refactorings in order to get some idea of the profitability of the refactoring's effort. A third way to exploit the metrics suite is to have it point out the part of code that would mostly benefit from a certain type of refactoring.

This thesis acts to find valuable indicators to measure the technical debt and maintainability focused on the real life context of a software company, where the research results will be deployed and their usefulness evaluated. The case company's software product has been developed since the 1970s and makes no exception in the matter of a code base becoming more complex over time, especially the core part of the product that is written in the Fortran language.  Although active quality work is now done in the development teams, the cost of the complex code base is paid in form of loss of productiveness of the developers in the maintenance work. The first and bigger part of this thesis will focus on providing a automatic metrics system that has relevant metrics depicting the case company's software products core's maintainability.

For the metrics to become useful and add value to the company's development process, they must be actively used by the development teams doing the refactoring work, but also by management in leading the quality work. Studies show that 78\% or more of measurement programs fail within the first two years after initiation \citep{dekkers1999secrets}. This underlines the importance of a well-thought deployment of metrics. Hence the other part of the thesis is to find out the most useful way to use the gathered data of the software's maintainability. What would be the most natural way for the case company's development teams and management to include the metrics as a part of their work and in this way to maximize value the metrics offer to them. What should the metrics look like? Where should they visible and when? How often should they be viewed? Should the case company have some team or company level practices regarding the use of the metrics?

The ultimate goal -- although outside of the scope of the thesis -- is to be able to make fact based decisions for the optimal prioritization of the development of features, bug fixes and improvements, which would consider long and short term business targets as well as the estimated productivity benefits gained from refactorings, see Figure \ref{fig:optimal_prioritization}. Measuring technical debt and especially maintainability is a challenging task and no exact metrics are possible. Even harder is to draw measure productivity. Hence this thesis tries to find those metrics that best depict the state of the case company's software's core. There is also no absolute correct way of using metrics. Therefore the way most fitting to the case company is looked for.

\begin{figure}[ht]	
	\begin{center}
		\includegraphics[width=\textwidth]{images/road_to_optimal_prioritization.png}
    		\caption{A model of how to form optimal prioritization of development work.}
    		\label{fig:optimal_prioritization}
  	\end{center}
\end{figure}

\section{Research questions}

The question to answer in the thesis is: \textit{How to track the case company product core's maintainability in a relevant and useful way in order to improve them?} The following two research questions are formed based on this higher level question.

\begin{itemize}
	\item RQ1: What are depicting metrics to measure the maintainability of the Fortran code base in the case company?
	\item RQ2: How to deploy the generated metrics in the development teams as a part of their normal sprint work?
\end{itemize}

Both of these questions are studied by using a literature review and empirical evidence. Two artifacts were be produced as a result of this research. The first is a automatic metrics system measuring relevant aspects of the case company's software product's maintainability and acts as an answer to RQ1. The second part answers RQ2 in form of guidelines of how to use and where to incorporate the metrics system once it exists.

\section{Structure of the Thesis}
\label{section:structure} 

Figure \ref{fig:thesis_structure} depicts the structure of the thesis and to which research question each chapter relates to by either providing background information or by researching for an answer to them.

\begin{figure}[ht]	
	\begin{center}
		\includegraphics[width=\textwidth]{images/thesis_structure.png}
    		\caption{How each chapter of the thesis relates to the research questions.}
    		\label{fig:thesis_structure}
  	\end{center}
\end{figure}

The Introduction chapter describes the research problem, goals and research questions which will be answered in the thesis. The following Maintainability chapter defines key terms related to maintainability and technical debt necessary to understand the thesis. The third chapter lists the results of a literature review done to find the metrics to measure technical debt and maintainability that are suggested in literature. Chapter four describes the case company, their products and the environment that the metrics are developed for. Chapter five describes the research methods used in the thesis.

Chapter six gives the results of the case study done on finding out suitable metrics and implementing an automatic metrics build based on the metrics found in the literature review and the interviews of the case study. In chapter seven good practices to deploy the metrics into the development teams and their ongoing quality work are researched using both a literature review and a case study. Chapter 8 evaluates the results and their validity. The Discussion chapter analyzes the results and puts them in a broader context. In the Conclusions chapter the most important findings are summarized.



