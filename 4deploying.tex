\chapter{Deploying the metrics}
\label{chapter:deploying}

Metrics -- no matter how well they have been chosen -- will not do any good if they are not used. Too often metrics are being calculated without being made use of. This is why it the project of creating a set of metrics does not end when the calculated metrics are at hand, but it is essential to plan and ensure the use of the metrics in the target groups. \citep{dekkers1999secrets}

\section{Practices for successful metrics deployment in literature}

The deployment practices suggested in literature were grouped into five categories depending on what part of the organization they relate to (individual developers, agile teams or the whole company), whether they relate to the metrics program itself or were technical solutions (Table \ref{tab:deploy_lit}). Most recommended practices were team practices and company wide practices. Each group and their practices listed in Table  table are \ref{tab:deploy_lit} are described in more detail in the subsections below.

\newpage
\begin{longtable}{p{0.17\textwidth}p{0.17\textwidth}p{0.17\textwidth}p{0.17\textwidth}p{0.17\textwidth}}
\caption{Deployment practices in literature}\\
\label{tab:deploy_lit}
\textbf{As part of individual developers' work process} & \textbf{As a part of agile teams' work process} & \textbf{Company wide practice} & \textbf{On metrics program} & \textbf{Technical solutions} \\ \hline
Metrics as help in finding refactoring targets \citep{stroggylos2007refactoring, simon2001metrics, wettel2008visually} & User stories for paying of technical debt \citep{brown2010enabling, bavani2012distributed} & Base decision of merging to master on metrics \citep{antinyan2014monitoring} & Ensure commitment by including personnel in metrics definition \citep{westfall200512, dekkers1999secrets} & Build fails when technical debt violations \citep{gat2012point} \\ \hline
Metrics as help in validating refactoring results \citep{stroggylos2007refactoring, simon2001metrics} & Fixed capacity in sprint for paying of technical debt \citep{power2013understanding} & Incentive structures \citep{iversen2001principles} & Test metrics with pilot version \citep{letouzey2012managing, iversen2001principles} & All metrics easily accessible in one place \citep{westfall200512, iversen2001principles, antinyan2014monitoring, dekkers1999secrets} \\ \hline
 & Talk about changes in metrics in retros / reviews \citep{bavani2012distributed} & Base metrics on company goals \citep{westfall200512} & Metrics chosen carefully since they steer work focus \citep{westfall200512} & \\ \hline
 & Team considers impact and trade-offs of taking and repaying technical debt \citep{bavani2012distributed, letouzey2012managing} & Management committed to make changes based on metrics and decision criteria \citep{westfall200512, dekkers1999secrets} & Define decision criteria \citep{westfall200512, dekkers1999secrets} &  \\ \hline
 & Set targets for the wanted level of the metrics \citep{letouzey2012managing, iversen2001principles, emam2002iso,  dekkers1999secrets} & Never used against individuals \citep{westfall200512, dekkers1999secrets, grady1992practical} &  &  \\ \hline
 & Teams identify and prioritize technical debt \citep{bavani2012distributed} & Facilitate debate \citep{iversen2001principles} &  &  \\ \hline
\end{longtable}

\subsection{As part of individual developers' work process}
Metrics \citep{stroggylos2007refactoring} and their visualizations \citep{simon2001metrics} can help the developer to find suitable targets for refactoring and afterwards to evaluate the effects of done refactorings, hence to do more systematic refactoring efforts. \citet{simon2001metrics} researched an approach where different aspects of code are visualized to help developers to spot anomalies in the code and hence find good candidates for refactoring.

\citet{wettel2008visually} observed that an even more useful approach than just listing or even visualizing metrics is to visualize the locations of the code base's basic features and its design problems. They researched the use of a software tool called CodeCity in visualizing a code base and its different aspects as a three-dimensional city (see Figure \ref{fig:codecity}). This tool provides the user a way to quickly comprehend the big picture of the code base and its possible design problems, such as code smells. These problems can after then be investigated closer and possibly be improved by refactoring.

\begin{figure}[!htbp]	
	\begin{center}
		\includegraphics[width=\textwidth]{images/codecity.png}
    		\caption{Code City, a tool visualizing a code base and its different aspects as a 3D city.}
    		\label{fig:codecity}
  	\end{center}
\end{figure}

\subsection{As a part of agile teams' work process}
A good practice in managing the technical debt is adding user stories related to paying off the technical debt into sprints \citep{brown2010enabling, bavani2012distributed} and releases \citep{brown2010enabling}. \citet{power2013understanding} recommends having a regular, fixed capacity of stories related to paying off technical debt in each sprint. Following the realized work types of each sprint is needed to ensure the continuous improving of the architecture instead of adding to the technical debt.

Development teams should be able to identify and prioritize technical debt and make informed, conscious decisions based on trade-offs when accumulating or reducing the amount of technical debt \citep{bavani2012distributed}. Accidentally added technical debt is recognized in reviews and retrospectives \citep{bavani2012distributed}. Although not literally noted in the article using metrics is an obvious way of tracking technical debt.

\subsection{Company wide practice}
\citet{letouzey2012managing} suggests a technical debt management practice to systematically reduce the technical debt. This practice includes defining and implementing technical debt metrics, setting targets for the desirable level and kind of technical debt, continuous monitoring of the debt, analyzing its impact and repaying the debt. The bad practices creating the technical debt that the organization wants to measure in the metrics are defined by experts from different units in a common workshop. The metrics are reviewed and updated annually in annual reviews.

\citet{iversen2001principles} describe incentive structures, determining goals, publishing objectives and data widely and facilitating debate as elements of a successful metrics program.

ISO-15939 \citep{emam2002iso} suggests defining a decision criteria, i.e. thresholds, targets or patterns of the metrics that indicate the need to act. A similar point is raised by \citet{dekkers1999secrets} and \citet{westfall200512}, who emphasize the importance to act upon the changes visible in metrics. Management needs to be committed to make changes based on the data presented by metrics.

In the process described by \citet{antinyan2014monitoring} self-organized agile teams use the metrics to support them in making more informed decisions when delivering code to the main branch of the version control system. The teams monitored trends in the metrics and observed that their attention was drawn to just a handful of files. Before merging changes to the master branch the changes especially in these key files were evaluated manually for their potential harmfulness. The research was done as a case study in two large enterprises. The metrics were quickly accessible in a MS Sidebar gadget in a simple form. The gadget also had links to more detailed statistics and other data.

Metrics should never be used against individuals, since this will typically negatively affect the validity of the data \citep{westfall200512, dekkers1999secrets, grady1992practical}. What must be meant with this is that the affected personnel will do everything possible to avoid the unpleasant incident of happening again -- even by cheating the metrics in a non-productive manner. Hence a more effective way would be to use the metrics to guide people in their work and show the progress in the measured things, or to emphasize positive changes in the measured subject and to give positive feedback for people based on their good work.

\subsection{On metrics program}
\citet{westfall200512, dekkers1999secrets} remind that software is a complex system and benefit of using a large set of metrics to depict its different important aspects. These metrics are best placed in one easily accessible location so that all aspects can be monitored easily \citep{dekkers1999secrets}. \citet{dekkers1999secrets} recommends clarifying the role of metrics for managers: that they depict the current state that can be used as a base for corrective actions, but that metrics as such are not the corrective action.

When defining the metrics it is important to include the persons who will be the ones using the metrics, since participating in the definition process builds the feeling of ownership of the metrics \citep{westfall200512, dekkers1999secrets}. This way the persons are more likely to use and maintain the metrics. Apart from the committing effect, participating the people involved in the measured work or product will produce valuable input, since they are experts on the topic being measured \citep{westfall200512}.

\citet{westfall200512} also notes that the metrics used should always be chosen carefully, since they will affect the behavior of the personnel working on the measured subject. People often want the metrics to look good and hence focus their work on the measured items, even sometimes in an unwanted way. Hence it is important that the metrics are based on the goals of the company.

\subsection{Technical solutions}
\citet{gat2012point} suggest having an automated measurement system for technical debt that is linked to the software build. The build would automatically fail on technical debt violations.

No matter how the metrics are implemented, according to \citet{westfall200512} for metrics data to be useful and used, it is necessary that the gathered data is easily accessible in a metrics database.

\section{Characteristics of useful metrics}
\citet{heitlager2007practical} lists characteristics of useful metrics. These are traceability, comprehensibility, the ease to compute, technology independence and classifying results and showing each class' distribution instead of showing average results of metrics' results.

In order for the developers to know the correct target to improve, the ability to trace the cause of the metric values and possible changes in them is relevant.

\clearpage
\begin{longtable}{p{0.3\textwidth}p{0.7\textwidth}}
\caption{Characteristics of useful metrics listed in literature}\\
\label{tab:characteristics_literature}
\textbf{Characteristic} & \textbf{Why?} \\ \hline
Traceability & In order to know the correct target to improve, the ability to trace the cause of the metric values and possible changes in them is relevant. \\ \hline
Comprehensibility & For the metrics to be applied and used correctly they have to be comprehensible and easily explainable. \\ \hline
Easy to compute & For the metrics to require little up-front investment, they should consist of simple definitions. \\ \hline
Technology independent & To be widely applicable to different systems, the metrics should be technology independent. \\ \hline
Distribution of severity classes instead of average metrics & Measuring average metrics tends to mask the outliers with high complexity values behind the large amount of low complexity getter and setter functions, etc. More representational is to measure the percentage of e.g. high complexity code of the whole code base. \\
\end{longtable}