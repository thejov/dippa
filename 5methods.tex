\chapter{Research method}
\label{chapter:methods}

This chapter describes the environment and the research paradigm used in the research. The environment of the thesis was the case company, for which the artifacts of the thesis research were implemented. The used research paradigm was Design Science, which aims in supporting the rigorous artifact development for research purposes.

\section{Environment: Case company}

The environment of the thesis research was a case company. The case organization is a software company headquartered in Helsinki, Finland, but also has development and sales offices around the world. The case company offers many different products and solutions in the area of naval engineering. The majority of the products are based on the same core component written in Fortran, which was initially developed already in the 1970s. During its long lifetime the core has undergone a great deal of changes. The Fortran language has also developed during this time allowing more sophisticated ways to solve problems. Small parts of the core have been rewritten in modern programming languages.

At the case company practically all Fortran code was written using the Fortran 77 standard. During the code's evolution more modern Fortran standards were applied in automatic mass refactorings, such as applying free format layout and explicit declarations for variables. Still a large amount of coding practices of Fortran 77 retain where newer more beneficial ones are available. Also some low level features like memory management have been implemented in-house, since they were not available at the time. These implementations are still in use and should be replaced by modern 3rd party solutions.

\subsection{Case selection}
The case selection enabled the study of how a Scrum organization can control the maintainability of a software product with the help of relevant software metrics. Underlining the topic's relevance is that the topic suggestion came from the case company itself due to the need for good enough metrics to depict the maintainability of their software product. If the resulting metrics system succeeds they might be included in the company's technology strategy in form of concrete targets for the metrics to improve the maintainability of the software. Hence the case organization was highly motivated to offer help and support for the research.

Before this thesis work the company has been measuring test coverage, average cyclomatic complexity of functions and the code base size. Apart from the test coverage metrics, the company wide use of the metrics has not been systematic and the need for a more effective way of deploying the existing metrics has been recognized. The need for more precise metrics measuring the maintainability of the software product's Fortran code base, especially the technical debt including Fortran code violations, has been identified and an initial list of the most common code violations and other bad practices has been made and fixes have been suggested for these. The progress of the refactoring work would be tracked and managed using metrics that are also suggested for each listed bad coding practice. The full list of suggested refactorings by the case company can be found in Appendix \ref{chapter:bad_design_listed_before}.

At the time of the research the company was doing the development work using the Scrum process on team level and the Scaled Agile Framework \citep{leffingwell2014scaled} on organization level. The company has 75 software developers and architects in 13 teams. Out of these less than 50 are working with the Fortran code base.

\section{Research paradigm: Design Science}

Design Science \citep{von2004design} was chosen as the research paradigm of this thesis. The goal of design science research is to develop an artifact that solves an organizational problem. The artifact may be in form of developed a software or it can be intellectual. The artifact has to be of novel nature. The problem to be solved in the research is defined by describing the current state and the goal. During the gap between the two is narrowed down in an iterative and incremental manner. For doing this the problem can be divided into several sub problems which are solved in each iteration. Hence the scope of the problem that the artifact solves can be extended iteratively.

After each iteration the artifact is evaluated using certain evaluation methods appropriate to the artifacts nature. If possible the research results are evaluated using a predefined set of metrics.  Research needs to be rigorous, but only up to the level in which is not clogging the relevance of the research. When the research is finished and the results presented, it is important to customize the level of detail and focus of the presentation depending on the audiences background: More technical details for a technical crowd and a more general presentation emphasizing the importance of the problem and the value of the solution for a more management-oriented audience.

The Design Science process consists of six steps: identifying the problem and its importance, defining the objectives of a solution, design and development of an artifact to meet defined objectives, a demonstration where the artifact is applied in a real life context, evaluation where the artifact's efficiency and effectiveness in the is evaluated based on the demonstration and finally the communication of the research in publications.

\subsection{Design Science in case study}

This research applied a simplified version of Design Science. The research process consisted of four steps as depicted in Figure \ref{fig:ds_process}. In the first step the problem was defined in form of the research questions and the research problem. Their relevance was underlined in the introduction chapter but also in the case company description. The second step was the artifact developments, which consisted of finding answers to the research questions at workshops at the case company and in literature and implementing or prototyping them. The third step was the evaluation, which was performed by presenting the results to case company personnel and having them evaluate them in an online survey. During the artifact development smaller evaluations were performed by the instructor after each of the three development iterations. The fourth step, communicating the results, was partly done in the evaluation session, but will be performed more thoroughly by the delivery of this masters thesis and by a more in-depth presentation at the case company after the delivery. The research results were also presented at the Aalto university to the supervising professor and other academic guests.

\begin{figure}[!htbp]	
	\begin{center}
		\includegraphics[width=\textwidth]{images/ds_process.png}
    		\caption{The simplified Design Science process applied in the research.}
    		\label{fig:ds_process}
  	\end{center}
\end{figure}

The research done has two artifacts: the automatic metrics build and the guidelines on how to deploy them in the teams. The metrics build consists of well-chosen metrics depicting the maintainability of the case company's Fortran code base. The generated metrics needed to be usable and easily deployable to be a part of the teams scrum work. To be relevant and to ensure their continuing use the deployment guidelines of the metrics needed to be simple, minimal and feel like a natural fit for the teams sprint work.

The metrics build created as a part of the thesis was a minimum viable product that could easily be extended in future. Hence an emphasis was on implementing the machinery for generating the metrics. New metrics can be added with minimal effort even after the thesis' research.

\section{Research process for data collection in workshop sessions}

The case study plan structure suggested by \citet{robson2002real} was used to plan the workshop sessions at the case company getting their input on good solutions for RQ1 and RQ2.

\subsection{Objective}
\begin{enumerate}
	\item Get case company personnel's answers to RQ1 and RQ2.
	\item Get case company personnel emotionally invested in the thesis research and its artifacts: the metrics build and its deployment.
\end{enumerate}

\subsection{Selection strategy}
The basis of who to include in the workshops was completely based on the fact of who will be using the metrics later and who's work it will be affecting. Developers working with the Fortran code base will be the most affected, since they will be the ones reading the metrics and focusing their refactoring efforts accordingly. Management will be using the metrics for prioritizing the issues the development team's will work on and setting targets. This can happen on team level when done for example in sprint planning sessions together with the product owner and the developers and on company level setting the company's technology strategy.

Hence the input gathering sessions were held for each of the five development teams that are working with the Fortran code base. Also a session was held to hear the Release \& Tools team and managers that are closest to R\&D. Separate sessions were held for each of these groups. The reasoning was based on experience of other meetings at the case company that have showed that the more participants a meeting has, the less feedback is to be expected from each participant. Smaller groups where the participants know each other well (teams) have gotten the most output from each participant.

\subsection{Methods}
Due to the large group of people to gather data from, the data collection method has to be efficient and lightweight. Individual interviews fall out of the scope due to their time consuming nature. Group interviews would be another option, but it is not the most effective way to activate and get the most output of each participant \citep{bjornson2009improving}. Methods that enable users to input silently by writing activate the participants well \citep{lehtinen2011development}, even better than only a group conversation \citep{bjornson2009improving, faure2004beyond}. This allows each person to participate at the same time without the need to fight for his turn to speak, e.g. in case of some participants dominating the discussion. Also the writing process forces everyone to actively process their own thoughts on the subject.

Since the teams are partly distributed between sites a tool enabling easy collaboration between sites is needed. The ARCA-tool is proven to be a efficient and easy to learn and use tool for conducting retrospectives for distributed teams \citep{lehtinen2014tool} and has been successfully used by a development team at the case company for almost two years in their sprint retrospectives. Since the case study is ultimately seeking to improve the case company's process of software maintenance a retrospective is a natural fit for the purpose. The process described by \citet{lehtinen2014tool} was originally used by the team but has evolved towards a more fitting match for them. The used input collection method was based on the evolved method of the team. The aim is to benefit from the team's lessons learned and get a more natural feeling retrospective method. As in the team's current retrospectives, communication is performed using the Microsoft Lync video conferencing tool.

The resulting process that was used in the sessions is the following:
\begin{enumerate}
	\item Intro, what are we going to do and how (5 min)
	\item Input collection: Maintenance difficulties, how to measure them (10 min)
	\item Going through input, Discussion  (15 min)\\
		\textit{Focus on unclear points, asking how listed problems could be measured}
	\item Prioritizing results by voting (2 min)\\
		\textit{Everyone has 3 votes}
	\item Input collection: How to deploy metrics (10 min)
	\item Going through input, Discussion  (15 min)
	\item Prioritizing results by voting (2 min)\\
		\textit{Everyone has 2 votes (due to generally lower amount of input)}
\end{enumerate}

The sessions were divided in two parts. The first part gathered the participants input on their views of RQ1 and the second part on RQ2. In other words the first part of the session aimed to answer the question on \textit{what} to measure and the second part on \textit{how} to use this data in order for it to be useful. Each part had the same amount of time allocated. The first part was approaching the question in a problem oriented way, asking the participants what problems they counter in their work with the Fortran code base and whether these could be measured. The initial ARCA-tool analysis can be seen in Figure \ref{fig:retro_initial_rq1}.

\begin{figure}[!htbp]	
	\begin{center}
		\includegraphics[width=0.5\textwidth]{images/retro_initial_rq1.png}
    		\caption{The base of the ARCA-tool analysis for RQ1, before the participants input.}
    		\label{fig:retro_initial_rq1}
  	\end{center}
\end{figure}

The second part aimed to gather the participants ideas on how the metrics should be deployed in order for them to be useful. Also feedback on the current metrics and their usefulness was gathered. The initial ARCA-tool analysis can be seen in Figure \ref{fig:retro_initial_rq2}. In order to give the participants a better understanding on what the ideas might be related to the following questions were verbally asked by the presenter:
\begin{itemize}
	\item What should the metrics look like?
	\item Where should they be visible?
	\item How often should the metrics be studied?
	\item Should we have some team practices for the use of the metrics?
	\item Should we have some company practices for the use of the metrics?
\end{itemize}

\begin{figure}[!htbp]	
	\begin{center}
		\includegraphics[width=0.5\textwidth]{images/retro_initial_rq2.png}
    		\caption{The base of the ARCA-tool analysis for RQ2, before the participants input.}
    		\label{fig:retro_initial_rq2}
  	\end{center}
\end{figure}

\section{Prioritization criteria for the collected data}

For choosing the most relevant ideas to implement, the results have to be prioritized. The research result funnel in Figure \ref{fig:research_data_funnel} depicts the different factors for prioritizing the results gathered from case company workshop sessions and literature. Out of the prioritized results the most relevant ones were chosen to implement as artifacts of the thesis research. The affecting factors for ideas are: the amount of overall votes, whether it was a top voted issues of a group, the amount of groups that voted for the idea or mentioned it, the ideas ease of implementation and whether it was suggested by literature.

\begin{figure}[[!htbp]	
	\begin{center}
		\includegraphics[width=\textwidth]{images/research_data_funnel.png}
    		\caption{The research result funnel: Prioritization of research results}
    		\label{fig:research_data_funnel}
  	\end{center}
\end{figure}

Since the artifacts will be developed for and used by the case company, the bigger weight in prioritization is given to the results of the workshop sessions. The artifact should be as useful as possible in specifically in the case company environment.

The prioritization criteria is based on the one introduced by \citet{lehtinen2014perceived}, who use four categories to sort the causes for problems perceived in project retrospectives. The classes are: process areas, cause types, interconnectedness and feasibility for process improvement. Process areas describe in which part of the organization or process the problems occur, e.g. Implementation, Software testing or Management. Cause types generalize the cause in whether it is related to people, tasks, methods or environment and which of their subcategories. The dimension of interconnectedness analyzes, which process areas are connected to each other. Feasibility for process improvement simply states whether the problem cause is concrete enough to be solved by the resources available.

The process area and cause class as such were not used since they did not fit the rather technical results of the workshop sessions. They seem to be better suited for general project or iteration retrospectives. A similar generalization as in the cause class was made, but a fitted version to the context: all issues were generalized and classified to one of the most commonly named causes. The used classes are listed in Appendix \ref{chapter:classification_lists}.

The issues were also classified according to their equivalent in literature. Ease of implementation was used instead of the feasibility for process improvement to describe whether a issue is feasible to implement in the scope of the thesis work. Interconnectedness of the suggested ideas was investigated to offer more insight of the issues and their relationships, but was not used for prioritization. The complete lists of issues and ideas that were listed by the groups and prioritized by them as relevant are visible in Chapter \ref{chapter:artifact_development} in Figure \ref{fig:retro_stats} and Figure \ref{fig:retro_stats_deploy}. These also contain their scores in the different classes used for prioritization.

\section{Artifact development process}
The artifact development was divided into three iterations. After each iteration a small evaluation followed by presenting the results to the thesis instructor at the case company. The next iteration was planned considering his feedback.

\section{Evaluation}

To evaluate the success of the thesis' artifacts a session was held and the same people that participated in the sessions surveying for answers to RQ1 and RQ2 were invited to join. In this video conference session the artifacts were presented and feedback was gathered using a web survey. The link to the feedback form was handed at the beginning of the session and the participants were asked to answer each question during the session, right after the corresponding artifact was presented. This made sure that the participants remember each artifact in question and also that they did not forget to answer the survey.

Apart from validating the usefulness of the artifacts, the session was held in order to commit the case company personnel more to the metrics project. The idea was that they would feel that their previous participation had an impact on the work. Also the artifacts would feel more like their own when they understand that the artifacts were created based on their ideas. A short time frame of 30 minutes was chosen in order to get as broad participation as possible.

\subsection{Feedback session structure}

\begin{enumerate}
	\item Short introduction to the topic: \textit{What is the thesis about? What is this session about?}
	\item Providing a link to the feedback questionnaire. Participants are asked to answer each question right after the corresponding artifact has been presented.
	\item Describing and showing the results of the previous held sessions and their prioritization.
	\item Presenting the artifacts.
\end{enumerate}

\subsection{Questionnaire questions}

The first four questions are multiple choice questions with answer alternatives from one to five using the Likert scale \citep{likert1932technique}. For example when asking about relevance the answers would be: "Very relevant'', "Somewhat relevant", "Neutral", "Not very relevant", "Not at all relevant". The same pattern applies for questions asking about the accuracy and usefulness. The questions asked for feedback on the relevance of the maintainability index, and hence the single metrics of which it consists of, the maintainability index formula and the deployment practices. The last two questions are open questions which asked for open textual feedback on the good and bad sides of the presented metrics system and the deployment practices.\\

\begin{enumerate}
	\item How relevant do you find the maintainability index?
	\item How accurate do you find the formula and weights used in calculating the maintainability index?
	\item How useful do you find the reactive deployment practice?
	\item How useful do you find the proactive deployment practice?
	\item What do you find especially good in the metrics proto? 
	\item What should be changed or improved in the metrics proto?
\end{enumerate}